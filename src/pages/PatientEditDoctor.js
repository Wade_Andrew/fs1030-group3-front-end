import { React, useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import styles from "./PatientEdit.module.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Header from "../components/Header";
import InputAdornment from "@mui/material/InputAdornment";
import AccountCircle from "@mui/icons-material/AccountCircle";
import Container from "@mui/material/Container";

const initialFormState = {
  name: "",
  medicalID: "",
  birthdate: "",
  address: "",
  medications: "",
  allergies: "",
};

const PatientEditDoctor = () => {
  const [formState, setFormState] = useState(initialFormState);
  const { name, medicalID, birthdate, address, medications, allergies } = formState;

  const navigate = useNavigate();

  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`http://localhost:3003/patients/get/${id}`)
      .then((response) => setFormState({ ...response.data[0] }));
  }, [id]);

  const updatePatient = async (event) => {
    event.preventDefault();
    await axios
      .put(`http://localhost:3003/patients/editPatient/${id}`, {
        name,
        medicalID,
        birthdate,
        address,
        medications,
        allergies,
      })
      .catch((err) => {
        console.log(err);
      });
    toast.success("Patient details updated", {
      position: "top-center",
      autoClose: 1800,
      closeOnClick: true,
      pauseOnHover: true,
      theme: "dark",
    });
    setTimeout(() => {
      navigate(`/viewPatientDoctor/${id}`);
    }, 1200);
  };

  const handelInputChange = (event) => {
    const { id, value } = event.target;
    setFormState({ ...formState, [id]: value });
  };

  const handleFocus = (event) => event.target.select();

  return (
    <div className={styles.editPatientWrapper}>
      <Header />
      <Container maxWidth="sm">
        <form className={styles.editForm} onSubmit={updatePatient}>
          <TextField
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <AccountCircle />
                </InputAdornment>
              ),
              style: { fontSize: "1.1rem" },
            }}
            sx={{ pt: 0.2 }}
            inputProps={{ style: { fontSize: "1.15rem" } }}
            InputLabelProps={{ style: { fontSize: "1.2rem", color: "rgb(10, 97, 205)" } }}
            margin="normal"
            id="name"
            name="name"
            size="small"
            fullWidth="true"
            required
            label="Name"
            value={name}
            onChange={handelInputChange}
            onFocus={handleFocus}
          />

          <TextField
            sx={{ pt: 0.2 }}
            inputProps={{ style: { fontSize: "1.15rem" } }}
            InputLabelProps={{ style: { fontSize: "1.2rem", color: "rgb(10, 97, 205)" } }}
            margin="normal"
            id="medicalID"
            name="medicalID"
            size="small"
            fullWidth="true"
            required
            label="Medical ID #"
            value={medicalID}
            onChange={handelInputChange}
            onFocus={handleFocus}
          />

          <TextField
            sx={{ pt: 0.2 }}
            inputProps={{ style: { fontSize: "1.15rem" } }}
            InputLabelProps={{ style: { fontSize: "1.2rem", color: "rgb(10, 97, 205)" } }}
            type="date"
            margin="normal"
            id="birthdate"
            name="birthdate"
            size="small"
            fullWidth="true"
            required
            label="Birthdate"
            value={birthdate}
            onChange={handelInputChange}
            onFocus={handleFocus}
          />

          <TextField
            sx={{ pt: 0.2 }}
            inputProps={{ style: { fontSize: "1.15rem" } }}
            InputLabelProps={{ style: { fontSize: "1.2rem", color: "rgb(10, 97, 205)" } }}
            margin="normal"
            id="address"
            name="address"
            size="small"
            fullWidth="true"
            required
            multiline="true"
            minRows={3}
            label="Address"
            value={address}
            onChange={handelInputChange}
            onFocus={handleFocus}
          />

          <TextField
            sx={{ pt: 0.2 }}
            inputProps={{ style: { fontSize: "1.15rem" } }}
            InputLabelProps={{ style: { fontSize: "1.2rem", color: "rgb(10, 97, 205)" } }}
            margin="normal"
            id="medications"
            name="medications"
            size="small"
            fullWidth="true"
            required
            label="Medications"
            value={medications}
            onChange={handelInputChange}
            onFocus={handleFocus}
          />

          <TextField
            sx={{ pt: 0.2 }}
            inputProps={{ style: { fontSize: "1.15rem" } }}
            InputLabelProps={{ style: { fontSize: "1.2rem", color: "rgb(10, 97, 205)" } }}
            margin="normal"
            id="allergies"
            name="allergies"
            size="small"
            fullWidth="true"
            required
            label="Allergies"
            value={allergies}
            onChange={handelInputChange}
            onFocus={handleFocus}
          />
          <div>
            <Button
              sx={{ mb: 0.5, mt: 1.5 }}
              type="submit"
              variant="contained"
              color="success"
              fullWidth="true"
            >
              Update patient details
            </Button>
            <Link style={{ width: "fit-content" }} to="/PatientPageDoctor">
              <Button
                sx={{ mb: 0.5, mt: 0.5 }}
                variant="contained"
                color="primary"
                fullWidth="true"
              >
                Back to patient list
              </Button>
            </Link>
          </div>
        </form>
      </Container>
    </div>
  );
};

export default PatientEditDoctor;
