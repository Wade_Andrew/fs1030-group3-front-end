import React from "react";
import PatientAddDoctor from "../components/PatientAddDoctor";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import PatientTableDoctor from "../components/PatientTableDoctor";
import { ConfirmProvider } from "material-ui-confirm";
import Header from "../components/Header";
import styles from "./PatientPage.module.css";
import axios from "axios";

const PatientPageDoctor = () => {
  // gotta get doctor ID saved through localstorage during login process
  let id = localStorage.getItem("user");
  const [doctorName, setDoctorName] = React.useState("name");

  React.useEffect(() => {
    axios.get(`http://localhost:3003/doctors/get/${id}`).then((response) => {
      setDoctorName(response.data[0].username);
    });
  }, [id]);

  return (
    <div className={styles.patientPageWrapper}>
      <Header />
      <Container maxWidth="xl">
        <h3 className={styles.welcome}>
          Welcome, Dr. <span className={styles.greetingSlug}>{doctorName}</span>. Use the tools on
          this page to manage patient records.
        </h3>
      </Container>
      <Container maxWidth="xl">
        <Grid container spacing={4} justifyContent="center">
          <Grid item lg={4} md={3.5} sm={12}>
            <PatientAddDoctor />
          </Grid>
          <Grid item lg={8} md={8.5} sm={12}>
            <ConfirmProvider>
              <PatientTableDoctor />
            </ConfirmProvider>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default PatientPageDoctor;
