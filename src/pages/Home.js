import { React } from "react";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import Header from "../components/Header";
import Footer from "../components/Footer";
import styles from "./Home.module.css";
import EastRoundedIcon from "@mui/icons-material/EastRounded";
import { Fade } from "@mui/material";

const Home = () => {
  return (
    <div className={styles.page}>
      <Header />
      <div className={styles.title}>
        <p className={styles.slideDown}>Putting Patients First</p>
        <Link to={"/"}>
          <Fade in timeout={1000} style={{ transitionDelay: 1500 }}>
            <Button
              sx={{ mt: 1, pl: 1.5, pr: 1.5 }}
              variant="contained"
              color="success"
              endIcon={
                <EastRoundedIcon
                  style={{
                    paddingBottom: "3px",
                  }}
                />
              }
            >
              Find out more
            </Button>
          </Fade>
        </Link>
      </div>
      <Footer />
      <div className={styles.mainContent}></div>
    </div>
  );
};

export default Home;
