import { React } from "react";
import PatientCardDoctor from "../components/PatientCardDoctor";
import { ConfirmProvider } from "material-ui-confirm";
import Header from "../components/Header";

const ViewPatientDoctor = () => {
  return (
    <div style={{ paddingBottom: "2.25rem" }}>
      <Header />
      <ConfirmProvider>
        <PatientCardDoctor />
      </ConfirmProvider>
    </div>
  );
};

export default ViewPatientDoctor;
