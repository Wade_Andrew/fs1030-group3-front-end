import { React, useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import styles from "./Login.module.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import logo from "../images/logo_transparent_background_cropped.png";

const Login = () => {
  const [userLogin, setUserLogin] = useState("");
  const [passwordLogin, setPasswordLogin] = useState("");

  const [loginMessage, setLoginMessage] = useState("");

  const redirect = useNavigate();

  const login = () => {
    if (userLogin === "" || passwordLogin === "")
      return setLoginMessage("Please enter your credentials");
    axios
      .post("http://localhost:3003/login", {
        username: userLogin,
        password: passwordLogin,
      })
      .then((response) => {
        if (response.data.isAdmin) return redirect("/PatientPageAdmin");
        if (response.data.id) {
          localStorage.setItem("user", response.data.id);
          return redirect("/PatientPageDoctor");
        }
        setLoginMessage(response.data.message);
      });
  };

  return (
    <div>
      <form className={styles.loginForm}>
        <Link to={"/"}>
          <div className={styles.logoBox}>
            <img src={logo} className={styles.loginLogo} alt="logo" />
          </div>
        </Link>
        <TextField
          type="text"
          size="small"
          fullWidth="true"
          required
          label="Username"
          placeholder="Enter username"
          sx={{ mb: 1, mt: 4 }}
          variant="standard"
          onChange={(event) => {
            setUserLogin(event.target.value);
          }}
        ></TextField>
        <TextField
          type="password"
          size="small"
          fullWidth="true"
          required
          label="Password"
          placeholder="Enter password"
          sx={{ mb: 3, mt: 2 }}
          variant="standard"
          onChange={(event) => {
            setPasswordLogin(event.target.value);
          }}
        ></TextField>
        <Button variant="contained" color="primary" onClick={login}>
          Login
        </Button>
        <h3 style={{ textAlign: "center", marginTop: "0.75rem", color: "red" }}>{loginMessage}</h3>
      </form>
    </div>
  );
};

export default Login;
