import { React } from "react";
import PatientCard from "../components/PatientCard";
import { ConfirmProvider } from "material-ui-confirm";
import Header from "../components/Header";

const ViewPatient = () => {
  return (
    <div style={{ paddingBottom: "2.25rem" }}>
      <Header />
      <ConfirmProvider>
        <PatientCard />
      </ConfirmProvider>
    </div>
  );
};

export default ViewPatient;
