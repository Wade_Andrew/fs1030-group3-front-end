import { React } from "react";
import PatientAdd from "../components/PatientAdd";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import PatientTable from "../components/PatientTable";
import { ConfirmProvider } from "material-ui-confirm";
import Header from "../components/Header";
import styles from "./PatientPage.module.css";

const PatientPageAdmin = () => {
  return (
    <div className={styles.patientPageWrapper}>
      <Header />
      <Container maxWidth="xl">
        <h3 className={styles.welcome}>
          Welcome, <span className={styles.greetingSlug}>Admin</span>. Use the tools on this page to
          manage patient records.
        </h3>
      </Container>
      <Container maxWidth="xl">
        <Grid container spacing={5} justifyContent="center">
          <Grid item lg={4} md={3.5} sm={12}>
            <PatientAdd />
          </Grid>
          <Grid item lg={8} md={8.5} sm={12}>
            <ConfirmProvider>
              <PatientTable />
            </ConfirmProvider>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default PatientPageAdmin;
