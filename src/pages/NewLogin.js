import React from "react";
import { useNavigate } from "react-router-dom";
import styles from "./Login.module.css";
import Button from "@mui/material/Button";
import logo from "../images/logo_transparent_background_cropped.png";

const NewLogin = () => {
  const redirect = useNavigate();

  const adminLogin = () => {
    redirect("/PatientPageAdmin");
  };

  const tylerLogin = () => {
    localStorage.setItem("user", 11);
    redirect("/PatientPageDoctor");
  };

  const wadeLogin = () => {
    localStorage.setItem("user", 1);
    redirect("/PatientPageDoctor");
  };

  return (
    <div>
      <form className={styles.loginForm}>
        <div className={styles.logoBox}>
          <img src={logo} className={styles.loginLogo} alt="logo" />
        </div>
        <Button sx={{ mb: 2.5, mt: 5 }} variant="contained" color="error" onClick={adminLogin}>
          ADMIN LOGIN
        </Button>
        <Button variant="contained" color="primary" onClick={tylerLogin}>
          Doctor Tyler LOGIN
        </Button>
        <Button sx={{ mt: 1.25 }} variant="contained" color="success" onClick={wadeLogin}>
          Doctor Wade LOGIN
        </Button>
      </form>
    </div>
  );
};

export default NewLogin;
