import { React, useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import styles from "./PatientCard.module.css";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import AssignmentIcon from "@mui/icons-material/Assignment";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { useConfirm } from "material-ui-confirm";
import Moment from "moment";

const PatientCard = () => {
  const [showPatient, setShowPatient] = useState({});
  const [notes, setNotes] = useState("");

  const confirm = useConfirm();

  const { id } = useParams();

  const history = useNavigate();

  useEffect(() => {
    axios
      .get(`http://localhost:3003/patients/get/${id}`)
      .then((response) => setShowPatient({ ...response.data[0] }));
  }, [id, notes]);

  const deletePatient = async (id) => {
    await confirm({
      title: "Are you sure you want to delete this patient?",
      confirmationText: "Delete",
      confirmationButtonProps: { color: "error", variant: "contained", size: "small" },
      cancellationButtonProps: { color: "primary", variant: "contained", size: "small" },
      dialogProps: { maxWidth: "xs" },
      contentProps: { color: "text" },
    }).then(() => {
      axios.delete(`http://localhost:3003/deletePatient/${id}`);
    });
    toast.success("Patient record deleted", {
      position: "top-center",
      autoClose: 1500,
      closeOnClick: true,
      pauseOnHover: true,
      theme: "dark",
    });
    setTimeout(() => {
      history("/PatientPageAdmin");
    }, 1000);
  };

  const addNote = (event) => {
    event.preventDefault();
    axios
      .put(`http://localhost:3003/patients/notes/${id}`, {
        notes,
      })
      .then(() => {
        setNotes("");
      })
      .catch((err) => {
        console.log(err);
      });
    toast.success("Note added to record", {
      position: "top-center",
      autoClose: 1200,
      closeOnClick: true,
      pauseOnHover: true,
      theme: "dark",
    });
  };

  const cardSubtitle = (
    <ul>
      <li>
        <strong>Medical ID# </strong>
        {showPatient.medicalID}
      </li>
      <li>
        <strong>Patient since: </strong>
        {Moment(showPatient.patientSince).format("LL")}
      </li>
    </ul>
  );

  return (
    <div>
      <Card className={styles.card} elevation={10}>
        <CardHeader
          avatar={
            <Avatar sx={{ width: 65, height: 65, bgcolor: "green" }}>
              <AssignmentIcon style={{ fontSize: 30 }} />
            </Avatar>
          }
          action={
            <div>
              <Link to={`/editPatient/${showPatient.id}`}>
                <Button size="small" color="success" variant="contained">
                  Edit patient
                </Button>
              </Link>
              <Button
                size="small"
                sx={{ ml: 1, mr: 0.75 }}
                color="error"
                variant="contained"
                onClick={() => deletePatient(showPatient.id)}
              >
                Delete
              </Button>
            </div>
          }
          titleTypographyProps={{ fontSize: 26, color: "primary" }}
          title={showPatient.name}
          subheaderTypographyProps={{ fontSize: 15, color: "text" }}
          subheader={cardSubtitle}
        />
        <CardContent className={styles.info}>
          <Typography style={{ fontSize: 15 }} color="text">
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <strong>
                  <u>Birthdate</u>
                </strong>
                <p className={styles.formattedText}>{Moment(showPatient.birthdate).format("LL")}</p>
                <strong>
                  <u>Address</u>
                </strong>
                <p className={styles.formattedText}>{showPatient.address}</p>
              </Grid>
              <Grid item xs={6}>
                <strong>
                  <u>Medications</u>
                </strong>
                <p className={styles.formattedText}>{showPatient.medications}</p>
                <strong>
                  <u>Allergies</u>
                </strong>
                <p className={styles.formattedText}>{showPatient.allergies}</p>
              </Grid>
            </Grid>
            <p>
              <strong>
                <u>Notes</u>
              </strong>
              <p className={styles.formattedText}>{showPatient.notes}</p>
            </p>
            <form onSubmit={addNote}>
              <TextField
                inputProps={{ style: { fontSize: "1.1rem" } }}
                InputLabelProps={{ style: { fontSize: "1.05rem" } }}
                sx={{ mb: 1 }}
                id="notes"
                name="notes"
                fullWidth="true"
                multiline="true"
                minRows={3}
                label="Update patient notes"
                placeholder="Enter patient note"
                required
                value={notes}
                onChange={(event) => setNotes(event.target.value)}
              />
              <Button
                type="submit"
                variant="contained"
                size="small"
                color="success"
                sx={{ mr: 1, mb: "-1rem" }}
              >
                Add note
              </Button>
              <Link to="/PatientPageAdmin">
                <Button size="small" variant="contained" color="primary" sx={{ mb: "-1rem" }}>
                  Back to patient list
                </Button>
              </Link>
            </form>
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
};

export default PatientCard;
