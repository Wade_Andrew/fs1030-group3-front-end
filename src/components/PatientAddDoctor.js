import { React, useState } from "react";
import { toast } from "react-toastify";
import Swal from "sweetalert2";
import axios from "axios";
import styles from "./PatientAdd.module.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import InputAdornment from "@mui/material/InputAdornment";
import AccountCircle from "@mui/icons-material/AccountCircle";

const initialFormState = {
  name: "",
  medicalID: "",
  birthdate: "",
  address: "",
  medications: "",
  allergies: "",
};

const PatientAddDoctor = () => {
  let doctorID = localStorage.getItem("user");

  const [formState, setFormState] = useState(initialFormState);
  const { name, medicalID, birthdate, address, medications, allergies } = formState;

  const handleSubmit = (event) => {
    event.preventDefault();
    if (medicalID.length !== 9) {
      Swal.fire({
        text: "Medical ID# must be 9 digits",
        icon: "error",
        width: "25rem",
        confirmButtonColor: "#1b9cd8",
      });
    } else {
      axios
        .post("http://localhost:3003/addPatient", {
          name,
          medicalID,
          birthdate,
          address,
          medications,
          allergies,
          doctorID,
        })
        .then(() => {
          setFormState(initialFormState);
        })
        .catch((err) => {
          console.log(err);
        });
      toast.success("Patient added to database", {
        position: "top-center",
        autoClose: 1500,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        theme: "dark",
      });
    }
  };

  const handelInputChange = (event) => {
    const { id, value } = event.target;
    setFormState({ ...formState, [id]: value });
  };

  return (
    <div>
      <form className={styles.addForm} onSubmit={handleSubmit}>
        <div className={styles.titleBox}>
          <h3>Add new patient</h3>
          <p>Enter patient details below to create new patient record</p>
        </div>
        <TextField
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <AccountCircle />
              </InputAdornment>
            ),
            style: { fontSize: "1.1rem" },
          }}
          InputLabelProps={{ style: { fontSize: "1.05rem" } }}
          margin="normal"
          id="name"
          name="name"
          size="small"
          fullWidth="true"
          required
          label="Patient name"
          placeholder="Enter name"
          value={name}
          onChange={handelInputChange}
        />

        <TextField
          inputProps={{ style: { fontSize: "1.1rem" } }}
          InputLabelProps={{ style: { fontSize: "1.05rem" } }}
          margin="normal"
          id="medicalID"
          name="medicalID"
          size="small"
          fullWidth="true"
          required
          label="Medical ID # (9 digits)"
          placeholder="9 digit number eg. 123456789"
          value={medicalID}
          onChange={handelInputChange}
        />

        <TextField
          inputProps={{ style: { fontSize: "1.1rem" } }}
          InputLabelProps={{ shrink: true, style: { fontSize: "1.15rem" } }}
          type="date"
          margin="normal"
          id="birthdate"
          name="birthdate"
          size="small"
          fullWidth="true"
          required
          label="Patient birthdate"
          placeholder="Enter birthdate"
          value={birthdate}
          onChange={handelInputChange}
        />

        <TextField
          inputProps={{ style: { fontSize: "1.1rem" } }}
          InputLabelProps={{ style: { fontSize: "1.05rem" } }}
          margin="normal"
          id="address"
          name="address"
          size="small"
          fullWidth="true"
          required
          multiline="true"
          minRows={3}
          label="Patient address"
          placeholder="Enter address"
          value={address}
          onChange={handelInputChange}
        />

        <TextField
          inputProps={{ style: { fontSize: "1.1rem" } }}
          InputLabelProps={{ style: { fontSize: "1.05rem" } }}
          margin="normal"
          id="medications"
          name="medications"
          size="small"
          fullWidth="true"
          required
          label="Patient medications"
          placeholder="Enter medications"
          value={medications}
          onChange={handelInputChange}
        />

        <TextField
          inputProps={{ style: { fontSize: "1.1rem" } }}
          InputLabelProps={{ style: { fontSize: "1.05rem" } }}
          margin="normal"
          id="allergies"
          name="allergies"
          size="small"
          fullWidth="true"
          required
          label="Patient allergies"
          placeholder="Enter allergies"
          value={allergies}
          onChange={handelInputChange}
        />

        <div>
          <Button
            sx={{ mt: 1.5 }}
            type="submit"
            variant="contained"
            color="success"
            fullWidth="true"
          >
            Add patient
          </Button>
        </div>
      </form>
    </div>
  );
};

export default PatientAddDoctor;
