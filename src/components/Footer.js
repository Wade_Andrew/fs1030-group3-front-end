import { React } from "react";
import styles from "./Footer.module.css";

const Footer = () => {
  return (
    <div>
      <p className={styles.footer}>Copyright 2022 - FS1030 - Group 3</p>
    </div>
  );
};

export default Footer;
