import { React } from "react";
import { Link } from "react-router-dom";
import styles from "./Header.module.css";
import { Box, Typography, Tooltip, Button } from "@mui/material";
import logo from "../images/logo_transparent_background_cropped.png";

const Header = () => {
  return (
    <div>
      <header className={styles.header}>
        <div className={styles.logoBox}>
          <Link to={"/"}>
            <img src={logo} className={styles.logo} alt="logo" />
          </Link>
        </div>
        <div className={styles.buttonBox}>
          <Link to={"/login"}>
            <Button
              sx={{ mr: 1.5, pt: 1, pb: 1, pl: 1.5, pr: 1.5 }}
              variant="contained"
              style={{ width: "fitContent" }}
              color="success"
            >
              Clinic Staff Login
            </Button>
          </Link>
        </div>
      </header>
      <Box className={styles.subHead}>
        <Tooltip title={<Typography fontSize={12}>Information for patients</Typography>}>
          <Link to={"/"}>
            <Typography variant="h6" color="primary">
              For Patients
            </Typography>
          </Link>
        </Tooltip>
        <Tooltip title={<Typography fontSize={12}>Care provider information portal</Typography>}>
          <Link to={"/"}>
            <Typography variant="h6" color="primary">
              For Care Providers
            </Typography>
          </Link>
        </Tooltip>
        <Tooltip title={<Typography fontSize={12}>Meet our doctors</Typography>}>
          <Link to={"/"}>
            <Typography variant="h6" color="primary">
              Our Doctors
            </Typography>
          </Link>
        </Tooltip>
        <Tooltip title={<Typography fontSize={12}>5 locations to serve you</Typography>}>
          <Link to={"/"}>
            <Typography variant="h6" color="primary">
              Locations
            </Typography>
          </Link>
        </Tooltip>
        <Tooltip title={<Typography fontSize={12}>Connect with our team</Typography>}>
          <Link to={"/"}>
            <Typography variant="h6" color="primary">
              Contact Us
            </Typography>
          </Link>
        </Tooltip>
      </Box>
    </div>
  );
};

export default Header;
