import { React, useState, useEffect } from "react";
import axios from "axios";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useConfirm } from "material-ui-confirm";
import TextField from "@mui/material/TextField";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import { toast } from "react-toastify";
import styles from "./PatientTable.module.css";
import InputAdornment from "@mui/material/InputAdornment";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import Moment from "moment";

const PatientTable = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const [patientData, setPatientData] = useState([]);
  const [search, setSearch] = useState([]);

  const confirm = useConfirm();

  const loadPatientData = async () => {
    const response = await axios.get("http://localhost:3003/patients/get");
    setPatientData(response.data);
  };

  useEffect(() => {
    // useEffect needs to be adjusted to prevent infinite loop //
    loadPatientData();
  });

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const deletePatient = async (id) => {
    await confirm({
      title: "Are you sure you want to delete this patient?",
      confirmationText: "Delete",
      confirmationButtonProps: { color: "error", variant: "contained", size: "small" },
      cancellationButtonProps: { color: "primary", variant: "contained", size: "small" },
      dialogProps: { maxWidth: "xs" },
      contentProps: { color: "text" },
    }).then(() => {
      axios.delete(`http://localhost:3003/deletePatient/${id}`);
    });
    toast.success("Patient record deleted", {
      position: "top-center",
      autoClose: 1200,
      closeOnClick: true,
      pauseOnHover: true,
      theme: "dark",
    });
    const response = await axios.get("http://localhost:3003/patients/get");
    setPatientData(response.data);
  };

  return (
    <div>
      <h3 className={styles.title}>Patient search</h3>
      <TextField
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchOutlinedIcon />
            </InputAdornment>
          ),
          style: {
            fontSize: "1.1rem",
            backgroundColor: "#e9e9e9",
            borderRadius: "4px",
            color: "black",
            opacity: 1,
          },
        }}
        variant="outlined"
        size="small"
        fullWidth="true"
        placeholder="Enter patient name or medical ID#..."
        onChange={(event) => setSearch(event.target.value)}
      />
      <br />
      <br />
      <Paper sx={{ width: "100%", boxShadow: 5, mb: 3 }}>
        <TableContainer sx={{ maxHeight: "75vh" }}>
          <Table stickyHeader className={styles.infoTable}>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Medical ID #</TableCell>
                <TableCell>Birthdate</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {patientData.length === 0 && (
                <tr>
                  <td colSpan="4">
                    <h3>No patient data</h3>
                  </td>
                </tr>
              )}
              {patientData
                .filter(
                  (patient) =>
                    patient.name.toLowerCase().startsWith(search) ||
                    patient.medicalID.includes(search)
                )
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((patient) => {
                  return (
                    <TableRow key={patient.id}>
                      <Link to={`/viewPatient/${patient.id}`}>
                        <td>{patient.name}</td>
                      </Link>
                      <td>{patient.medicalID}</td>
                      <td>{Moment(patient.birthdate).format("LL")}</td>
                      <td className={styles.actionColumn}>
                        <Link to={`/viewPatient/${patient.id}`}>
                          <Button sx={{ m: 0.25 }} variant="contained" color="primary" size="small">
                            View
                          </Button>
                        </Link>
                        <Link to={`/editPatient/${patient.id}`}>
                          <Button sx={{ m: 0.25 }} variant="contained" color="success" size="small">
                            Edit
                          </Button>
                        </Link>
                        <Button
                          sx={{ m: 0.25 }}
                          variant="contained"
                          color="error"
                          size="small"
                          onClick={() => deletePatient(patient.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 20, 30]}
          component="div"
          count={patientData.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};

export default PatientTable;
