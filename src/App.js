import { ToastContainer } from "react-toastify";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import PatientPageAdmin from "./pages/PatientPageAdmin";
import PatientPageDoctor from "./pages/PatientPageDoctor";
import PatientEdit from "./pages/PatientEdit";
import PatientEditDoctor from "./pages/PatientEditDoctor";
import ViewPatient from "./pages/ViewPatient";
import ViewPatientDoctor from "./pages/ViewPatientDoctor";
import Login from "./pages/Login";
import NewLogin from "./pages/NewLogin";
import Home from "./pages/Home";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <ToastContainer
          toastStyle={{ padding: "1.25rem", fontSize: "1.1rem" }}
          position="top-center"
          autoClose={1500}
          hideProgressBar={false}
          closeOnClick
        />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/PatientPageAdmin" element={<PatientPageAdmin />} />
          <Route path="/PatientPageDoctor" element={<PatientPageDoctor />} />
          <Route path="/editPatient/:id" element={<PatientEdit />} />
          <Route path="/editPatientDoctor/:id" element={<PatientEditDoctor />} />
          <Route path="/viewPatient/:id" element={<ViewPatient />} />
          <Route path="/viewPatientDoctor/:id" element={<ViewPatientDoctor />} />
          {/* <Route path="/newLogin" element={<NewLogin />} /> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
